import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import org.openqa.selenium.WebDriverException;

public class Driver {
	private static JFrame mainFrame;
	private static JLabel headerLabel;
	private static JLabel statusLabel;
	private static JPanel controlPanel;
	private static JPanel credentialsPanel;

	static JButton startButton;
	static JButton fileButton;
	static JButton cancelButton;
	
	static JTextField usernameField;
	static JPasswordField passwordField;

	static List <Entry> entries; 
	static Excel excel;
	static Selenium selenium;
	static Worker<Void, String> worker;

	static enum STATUS {LOGINFAIL, NOCONTACT, NOFILE, FILESELECTED, RUNNING, PAUSED, FILECOMPLETED, CANCELLED};	
	static STATUS status = STATUS.NOFILE; 

	static Thread thread = null;

	public static void main(String[] args) throws IOException {
		excel = new Excel();
		selenium = new Selenium();

		init();
		launch();
	}

	public static void init() {
		mainFrame = new JFrame("Vaillant Registering System");
		mainFrame.setSize(400,400);
		mainFrame.setLayout(new GridLayout(4, 1));

		headerLabel = new JLabel("",JLabel.CENTER );
		statusLabel = new JLabel("",JLabel.CENTER);        
		statusLabel.setSize(350,100);

		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent){	
				if (worker != null && !worker.isDone()) {
					worker.resume();
					worker.cancel(false);
				}
				
				while (true) {
					if (status == STATUS.CANCELLED || status == STATUS.NOFILE 
							|| status == STATUS.FILESELECTED || status == STATUS.FILECOMPLETED) {
						break;
					} 
					
					System.out.println("Waiting for worker cancel...");
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
				closeResource();
				System.exit(0);
			}        
		});    

		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());
		
		credentialsPanel = new JPanel();
		credentialsPanel.setLayout(new FlowLayout());

		mainFrame.add(headerLabel);
		mainFrame.add(controlPanel);
		mainFrame.add(credentialsPanel);
		mainFrame.add(statusLabel);
		mainFrame.setVisible(true);  
	}

	private static void launch(){
		headerLabel.setText("Please select excel file"); 

		startButton = new JButton("Start");
		fileButton = new JButton("Select File");
		cancelButton = new JButton("Cancel");

		startButton.setActionCommand("Start");
		fileButton.setActionCommand("Select File");
		cancelButton.setActionCommand("Cancel");

		startButton.setEnabled(false);
		cancelButton.setEnabled(false);

		ButtonClickListener listener = new ButtonClickListener();
		startButton.addActionListener(listener); 
		fileButton.addActionListener(listener);
		cancelButton.addActionListener(listener); 

		controlPanel.add(startButton);
		controlPanel.add(fileButton);
		controlPanel.add(cancelButton);    
		
		usernameField = new JTextField(20);
		passwordField = new JPasswordField(20);
		credentialsPanel.add(usernameField);
		credentialsPanel.add(passwordField);

		mainFrame.setVisible(true);  
	}

	private static class ButtonClickListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();  

			// Start
			if( command.equals( "Start" ))  {
				if (status == STATUS.FILESELECTED) {
					String username = usernameField.getText();
					String password = new String(passwordField.getPassword());
					if (username.isEmpty() || password.isEmpty()) {
						headerLabel.setText("Please enter username and password");
						return;
					}
					
					fileButton.setEnabled(false);
					worker = new Worker<Void, String>(username, password);
					worker.execute();
					worker.addPropertyChangeListener(new PropertyChangeListener() {

						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if ("loginfail".equals(evt.getPropertyName())) {
								if ((Boolean)evt.getNewValue() == true) {
									headerLabel.setText("Wrong username or password"); 
									startButton.setText("Start");
									cancelButton.setEnabled(false);
									fileButton.setEnabled(true);
									status = STATUS.FILESELECTED;
								}
							}
							
							if ("paused".equals(evt.getPropertyName())) {
								if ((Boolean)evt.getNewValue() == true) {
									headerLabel.setText("Operation Paused"); 
								}
							}

							if ("nocontact".equals(evt.getPropertyName())) {
								if ((Boolean)evt.getNewValue() == true) {
									startButton.setEnabled(true);
									startButton.setText("Start");
									status = STATUS.FILESELECTED;
									headerLabel.setText("Lost contact with browser, please restart");
								}
							}
							
							if ("cancelled".equals(evt.getPropertyName())) {
								if ((Boolean)evt.getNewValue() == true) {
									resetButtons();
									closeResource();
									statusLabel.setText("");
									headerLabel.setText("Operation Cancelled");
									status = STATUS.NOFILE;
								}
							}
						}
					});

					status = STATUS.RUNNING;
					startButton.setText("Pause");
					cancelButton.setEnabled(true);
					headerLabel.setText("Running..."); 
				} else if (status == STATUS.RUNNING) {
					worker.pause();
					headerLabel.setText("Pausing..."); 
					startButton.setText("Resume");
					status = STATUS.PAUSED;
				} else if (status == STATUS.PAUSED) {
					worker.resume();
					headerLabel.setText("Running..."); 
					startButton.setText("Pause");
					status = STATUS.RUNNING;
				}

				// Select File
			} else if( command.equals( "Select File" ) )  {
				boolean isValidFile = excel.setFile();
				if (isValidFile) {
					try {
						entries = excel.parseExcel();
						statusLabel.setText("File: " + excel.file.getName());

						if (entries.size() > 0) {
							status = STATUS.FILESELECTED;
							startButton.setEnabled(true);
							headerLabel.setText("Ready to start");	
						} else {
							status = STATUS.FILECOMPLETED;
							headerLabel.setText("Registration complete");
						}

					} catch (FileNotFoundException e1) {
						headerLabel.setText("File not found");
					} catch (IOException e2) {
						e2.printStackTrace();
					} 
				} else {
					headerLabel.setText("File not found");
				}
				
				// Cancel file
			} else if ( command.equals("Cancel") ) {				
				headerLabel.setText("Cancelling...");
				startButton.setEnabled(false);
				
				if (status == STATUS.RUNNING || status == STATUS.PAUSED) {
					worker.resume();
					worker.cancel(false);
				} else if (status == STATUS.NOCONTACT || status == STATUS.FILESELECTED) {
					resetButtons();
					closeResource();
					headerLabel.setText("Cancelled");
				}
			}
		}		
	}

	private static void resetButtons() {
		startButton.setEnabled(false);
		startButton.setText("Start");
		fileButton.setEnabled(true);
		cancelButton.setEnabled(false);
	}
	
	private static void closeResource() {
		try {
			excel.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (selenium.webDriver != null) {
			selenium.webDriver.quit();
		}
		
	}
	
	private static class Worker<K, V> extends SwingWorker<K, V> {

		private String username;
		private String password;
		private volatile boolean isPaused;

		Worker(String username, String password) {
			this.username = username;
			this.password = password;
		}
		
		@Override
		protected K doInBackground() throws InterruptedException {
			int exceptionCount = 0;
			int exceptionLimit = 3;

			try {
				boolean login = selenium.init(username, password);
				if (!login) {
					status = STATUS.LOGINFAIL;
				}
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			} catch (WebDriverException | NullPointerException e2) {
				System.out.println("selenium.init(): Error initiating browser");
				status = STATUS.NOCONTACT;
			}

			while (entries.size() > 0 && status != STATUS.NOCONTACT && status != STATUS.LOGINFAIL) {
				Iterator<Entry> iterator = entries.listIterator();
				while (iterator.hasNext() && status != STATUS.NOCONTACT) {     

					// Check Pause
					if (isPaused()) {
						System.out.println("Pausing...");
						firePropertyChange("paused", false, true);
						while (isPaused()) {
							Thread.sleep(1000);
						}
					}
					
					// Check Cancel
					if (isCancelled()) {
						status = STATUS.CANCELLED;
						System.out.println("Cancelling...");
						firePropertyChange("cancelled", false, true);
						firePropertyChange("cancelled", true, true);
						return null;
					}

					Entry entry = iterator.next();

					Selenium.RESULT seleniumResult = null;
					boolean result = true;	// false: entry already registered previously

					// Re-register if focus is lost due to user interaction with browser
					while (true && status != STATUS.NOCONTACT) {
						try {
							seleniumResult = selenium.register(entry);
							exceptionCount = 0;
							break;
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						} catch (WebDriverException e2) {
							System.out.println("WebDriverException: window lost focus, re-registering...\n");
							exceptionCount++;
							if (exceptionCount == exceptionLimit) {
								status = STATUS.NOCONTACT;
								System.out.println("WebDriverException: lost contact with browser");
							}
						} catch (NullPointerException e3) {
							System.out.println("NullPointerException: lost contact with browser");
							status = STATUS.NOCONTACT;
						}
					}

					if (status != STATUS.NOCONTACT) {
						if (seleniumResult == Selenium.RESULT.SUCCESS) {
							result = true;
						} else if (seleniumResult == Selenium.RESULT.REPEATED) {
							result = false;
						} else if (seleniumResult == Selenium.RESULT.FAILURE) {
							System.out.println("Skipping faulty registration...\n");
						}

						if (seleniumResult != Selenium.RESULT.FAILURE) {
							try {
								excel.writeResult(entry.excelRow, result);
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							iterator.remove();
						}
					}
				}
			}

			if (status == STATUS.NOCONTACT) {
				firePropertyChange("nocontact", false, true);
				firePropertyChange("nocontact", true, false);
			} else if (status == STATUS.LOGINFAIL) {
				firePropertyChange("loginfail", false, true);
				firePropertyChange("loginfail", true, false);
			} else {
				status = STATUS.FILECOMPLETED;
			}

			if (selenium.webDriver != null) {
				selenium.webDriver.quit();
			}

			return null;
		}

		@Override
		protected void done() {
			if (status == STATUS.FILECOMPLETED) {
				resetButtons();
				closeResource();
				headerLabel.setText("Registration Complete");
			}
		}

		public void pause() {
			if (!isPaused() && !isDone()) {
				isPaused = true;
			}
		}

		public void resume() {
			if (isPaused() && !isDone()) {
				isPaused = false;
				firePropertyChange("paused", true, false);
			}
		}

		public boolean isPaused() {
			return isPaused;
		}

	}

}
