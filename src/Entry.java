import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Entry {
	boolean submitted;
	int excelRow;
	
	static final String COMPANY = "上海依冬暖通设备工程有限公司";
	
	private String code;
	private String prodName;
	
	private String installDate;
	
	private String requestDate;
	
	private String city;
	private String county;
	private String district;
	private String quarter;
	private String street;
	private String subStreet;
	private String apartmentNo;
	
	private String client;
	
	private String phoneA;
	private String phoneB;
	
	public Entry() {
		Date today = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(today);
		c.add(Calendar.DATE, 1);
		Date tomorrow = c.getTime();
		this.requestDate = new SimpleDateFormat("yyyy/MM/dd").format(tomorrow);
	}
	
	/* Setter */
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}	
	
	public void setInstallDate(String installDate) {
		this.installDate = installDate;
	}
	
	public void setRequestDate(String requestDate) {
		if (!requestDate.isEmpty()) {	// Overwrite default value set in constructor
			this.requestDate = requestDate;
		}
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setCounty(String county) {
		this.county = county;
	}
	
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public void setClient(String client) {
		this.client = client;
	}
	
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setSubStreet(String subStreet) {
		this.subStreet = subStreet;
	}
	
	public void setApartmentNo(String apartmentNo) {
		this.apartmentNo = apartmentNo;
	}
	
	public void setPhoneA(String phoneA) {
		this.phoneA = phoneA;
	}
	
	public void setPhoneB(String phoneB) {
		this.phoneB = phoneB;
	}
	
	
	/* Getter */
	public String getCode() {
		return this.code;
	}
	
	public String getProdName() {
		return this.prodName;
	}
	
	public String getCodedProdName() {
		String codePortion = this.code.substring(6, 16);
		return codePortion + "...." + this.prodName;
	}

	public String getInstallDate() {
		return this.installDate;
	}

	public String getCity() {
		return this.city;
	}
	
	public String getRequestDate() {
		return this.requestDate;
	}

	public String getCounty() {
		return this.county;
	}

	public String getDistrict() {
		return this.district;
	}
	
	public String getQuarter() {
		return this.quarter;
	}
	
	public String getStreet() {
		return this.street;
	}
	
	public String getClient() {
		return this.client;
	}
	
	public String getSubStreet() {
		return this.subStreet;
	}
	
	public String getApartmentNo() {
		return this.apartmentNo;
	}

	public String getPhoneA() {
		return this.phoneA;
	}
	
	public String getPhoneB() {
		return this.phoneB;
	}
}
