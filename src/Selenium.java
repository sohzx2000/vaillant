import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.NoSuchElementException;

public class Selenium {
	WebDriver webDriver;
	static enum RESULT {SUCCESS, REPEATED, FAILURE};
	
	public boolean init (String user, String password) throws InterruptedException, WebDriverException {
		//System.setProperty("webdriver.ie.driver","C:/Users/UserPC/Downloads/IEDriverServer_x64_3.5.1/IEDriverServer.exe");
		//System.setProperty("webdriver.chrome.driver", "C:/Users/UserPC/Downloads/chromedriver_win32/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
	
		//Initialize browser
		//webDriver = new InternetExplorerDriver();
		webDriver = new ChromeDriver();
		
		// Maximize the browser window
		try {
			webDriver.manage().window().maximize();
		} catch (WebDriverException e) {
			System.out.println("WebDriverException: Failed to maximize\n");
		}
		
		// Open web page
		webDriver.navigate().to("http://dealers.vaillant.com.cn");

		// Log-in
		waitLoad("usernameTextBox", "id");
		webDriver.findElement(By.id("usernameTextBox")).sendKeys(user);
		webDriver.findElement(By.id("passwordTextBox")).sendKeys(password);
		webDriver.findElement(By.id("okButton")).click();
		
		if (webDriver.findElements(By.cssSelector("span.error")).size() > 0) {
			System.out.println("Login credentials incorrect");
			return false;
		}
		return true;
	}

	public RESULT register (Entry entry) throws InterruptedException, WebDriverException {
		System.out.println("Registering excelRow: " + (entry.excelRow + 1) + ": " + entry.getCode()); // non 0 based counting
		
		// Proceed to entry
		waitLoad("创建安装检查", "linkText");
		webDriver.findElement(By.linkText("创建安装检查")).click();
		
		// wait for form load
		waitLoad("ctl00_contentFacet_okButton", "id");
		
		// Input code
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_assemblyInformationControl_serialNumberTextBox")).sendKeys(entry.getCode());
				
		
		// Select product
		Select prodDropDown = new Select(webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_assemblyInformationControl_productCombo_popup")));
		selectOption(prodDropDown, entry.getCodedProdName());	
		
		/*List <WebElement> optionsInnerText= webDriver.findElements(By.xpath("//select[@id = 'ctl00_contentFacet_activationRequestControl_assemblyInformationControl_productCombo_popup']/option"));

	    for(WebElement text: optionsInnerText){
		    String textContent = text.getAttribute("textContent");
	
		    if (textContent.toLowerCase().contains(entry.getProdName().toLowerCase())) {
		    	 dropdown.selectByVisibleText(textContent);
		    	 break;
		    }
	    }*/
		
		
		// Select Install Date
		String installDate = entry.getInstallDate();
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_assemblyInformationControl_assemblyControl_dateDatePicker")).sendKeys(installDate);
		
		
		// Select Company
		Select coyDropDown = new Select(webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_assemblyInformationControl_assemblyControl_assemblerResellerComboBoxControl_popup")));
		selectOption(coyDropDown, Entry.COMPANY);
		
		
		// Select Request Date
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_serviceRequestControl_serviceRequestDatePicker")).sendKeys(entry.getRequestDate());;
		
		
		// Input Address
			// city, county, district
		WebElement city = webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_cityCombo_textfield"));
		WebElement county = webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_countyCombo_popup"));
		WebElement district = webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_districtCombo_popup"));
		
		waitEnabled(city, entry.getCity());
		waitEnabled(county, entry.getCounty());
		waitEnabled(district, entry.getDistrict());
		
			// quarter, street
		waitLoad("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_quarterCombo_popup", "id");
		waitLoad("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_streetCombo_popup", "id");
		Select quarterDropDown = new Select(webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_quarterCombo_popup")));
		Select streetDropDown = new Select(webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_streetCombo_popup")));
		
		selectOption(quarterDropDown, entry.getQuarter());
		selectOption(streetDropDown, entry.getStreet());	
		
			// sub street
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_subStreetTextBox")).sendKeys("");
		
			// apartment number
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_addressPopulatorControl_apartmentNumberTextBox")).sendKeys(entry.getApartmentNo());
		
		
		// Input Client
			// name
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_customerControl_fullNameTextBox")).sendKeys(entry.getClient());;
		
			// check "same address"
		WebElement addressCheckBox = webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_customerAddressControl_customerAddressBaseControl_sameAsProductInstanceAddressCheckBox"));
		addressCheckBox.click();
		
			// mobile phone
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_customerAddressControl_customerAddressBaseControl_mobileTelephoneTextBox_AreaCode")).sendKeys(entry.getPhoneA());
		webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_customerAddressControl_customerAddressBaseControl_mobileTelephoneTextBox_PhoneNumber")).sendKeys(entry.getPhoneB());
		
		
		// Check auto fill is complete first
		Select select = new Select(webDriver.findElement(By.id("ctl00_contentFacet_activationRequestControl_customerAddressControl_customerAddressBaseControl_countyComboBoxControl_popup")));
		WebElement option;
		
		while (true) {
			try {
				option = select.getFirstSelectedOption();
				break;
			} catch (NoSuchElementException e) {
				System.out.println("NoSuchElementException: waiting to check autofill completion...");
				if (!addressCheckBox.isSelected()) {
					addressCheckBox.click();
				}
			}
		}
		
		for (int i = 0; (!option.getText().equals(entry.getCounty()) && i < 5); i++) {
			Thread.sleep(100);
			System.out.println("Text: " + option.getText());
		}
		
		// Submit
		webDriver.findElement(By.id("ctl00_contentFacet_okButton")).click();
		
		// Check pre-alert input errors
		if (checkErrors()) {return RESULT.FAILURE;}
		
		while (true) {
			Alert alert;
			try {
				alert = webDriver.switchTo().alert();
			} catch (NoAlertPresentException e) {
				Thread.sleep(500);
				continue;
			}
			alert.dismiss();
			//alert.accept();
			break;
		}
		
		// Check post-alert input errors
		if (checkErrors()) {return RESULT.FAILURE;}
		
		// Check success/resubmit
		waitLoad("#ctl00_messageSummary", "cssSelector");
		WebElement resultSpan = webDriver.findElement(By.cssSelector("#ctl00_messageSummary"));
		String result = resultSpan.getText();
		System.out.println("Result: " + result + "\n");
		
		// return true/false
		if (result.contains("安装检查记录(单条)成功创建")) {
			System.out.println("Success\n");
			return RESULT.SUCCESS;
		} else if (result.contains("设备已经调试过")) {
			System.out.println("Repeated\n");
			return RESULT.REPEATED;
		}
		
		return RESULT.SUCCESS;
	}
	
	private boolean checkErrors() {
		boolean error= false;
		try {
			error = webDriver.findElements(By.cssSelector("#ctl00_validationSummary li")).size() > 0;
		} catch (WebDriverException e) {
			System.out.println("Input valid");
		}
		
		if (error) {
			System.out.println("Error in Input");
			return true;
		}
		return false;
	}
	
	private void selectOption(Select select, String text) throws InterruptedException, WebDriverException {
		int loopLimit = 10;
		int loopInterval= 250;
		
		for (int i = 0; i < loopLimit; i++) {
			try {
				select.selectByVisibleText(text);
				return;
			} catch (NoSuchElementException e) {
				Thread.sleep(loopInterval);
			}
		}
		
		throw new WebDriverException();
	}
	
	private void waitEnabled(WebElement element, String s) {
		WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10000);
		
		while(true) {
			webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
			
			if (element.isEnabled()) {
				element.sendKeys(s);
				break;
			}
		}
	}
	
	private void waitLoad(String identifier, String type) throws InterruptedException, WebDriverException {
		System.out.println("Waiting for: " + identifier);
		int loopInterval = 250;
		int loopCount = 0;
		int loopLimit = 10;
	
		while(countElements(identifier, type) < 1) {
			Thread.sleep(loopInterval);
			System.out.println("Wait: " + loopCount);
			if (++loopCount == loopLimit) {
				throw new WebDriverException();
			}
		}
	}
	
	private int countElements(String identifier, String type) {
		int elementCount = 0;
		switch (type) {
			case "id":
				elementCount = webDriver.findElements( By.id(identifier) ).size();
				break;
			case "linkText":
				elementCount = webDriver.findElements( By.linkText(identifier) ).size();
				break;
			case "cssSelector":
				elementCount = webDriver.findElements( By.cssSelector(identifier) ).size();
		}
		return elementCount;
	}
	
	private void dropHelper(String selectId, String targetText) {
		List <WebElement> options = webDriver.findElements(By.xpath("//select[@id = '" + selectId + "']/option"));
		
		int count = 0;
		for(WebElement visibleText: options){
		    String textContent = visibleText.getAttribute("textContent");
		    if (textContent.equals(targetText)) {
		    	 break;
		    } else {
		    	count++;
		    }
	    }
		
		WebElement select = webDriver.findElement(By.id(selectId));
		for (int i = 0; i < count; i++) { select.sendKeys(Keys.ARROW_DOWN); }	
	}
}
