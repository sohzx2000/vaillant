import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	
	File file = null;
	FileInputStream fis = null;
	XSSFWorkbook workbook;
	XSSFSheet spreadsheet;
	XSSFCellStyle successStyle;
	XSSFCellStyle repeatedStyle;
	XSSFRow row;
	List <Entry> entries;
	
	boolean setFile() {
		 JFileChooser chooser = new JFileChooser();
         FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel file", "xlsx", "xls");
         chooser.setFileFilter(filter);
         
         int returnVal = chooser.showOpenDialog(null);
         if (returnVal == JFileChooser.APPROVE_OPTION) {
        	 file = chooser.getSelectedFile();
        	 if (file.exists()) {
        		 System.out.println("You chose to open this file: " + file.getName());
            	 return true;
        	 }
         }
         
         return false;
	}
	
	void writeResult(int rowNum, boolean result) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);
		Row currentRow = spreadsheet.getRow(rowNum);
		Cell cell = currentRow.getCell(2);
		
		if (result == true) {
			cell.setCellStyle(successStyle);	
		} else if (result == false) {
			cell.setCellStyle(repeatedStyle);	
		}
		
		
		workbook.write(fos);
	}
	
	private void setStyle() throws IOException {
		successStyle = workbook.createCellStyle();
		successStyle.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
		successStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		repeatedStyle = workbook.createCellStyle();
		repeatedStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		repeatedStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	}
	
	List<Entry> parseExcel() throws FileNotFoundException, IOException {
		fis = new FileInputStream(file);
		workbook = new XSSFWorkbook(fis);
		spreadsheet = workbook.getSheetAt(0);
		setStyle();
		
		Iterator < Row > rowIterator = spreadsheet.iterator();
		List <Entry> entries = new ArrayList<Entry>();
		
		rowIterator.next(); // Skip first row (column labels)
		int excelRow = 1;
		while (rowIterator.hasNext()) {
			Entry entry = new Entry();
			row = (XSSFRow) rowIterator.next();
			Iterator < Cell > cellIterator = row.cellIterator();
			
			int count = 1;
			while ( cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				cell.setCellType(Cell.CELL_TYPE_STRING);
				
			    switch (count) {
			    	case 2:
			    		//System.out.print(cell.getStringCellValue() + " \t\t " );
			    		entry.setProdName(cell.getStringCellValue());
			    		break;
			    	case 3:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setCode(cell.getStringCellValue());
			        	if (cell.getCellStyle().getFillForegroundColor() == IndexedColors.BRIGHT_GREEN.getIndex()) {
			        		entry.submitted = true;
			        	}
			        	break;
			    	case 4:
			    		//System.out.print(cell.getStringCellValue() + " \t\t " );
			    		entry.setInstallDate(cell.getStringCellValue());
			            break;
			    	case 5:
			    		//System.out.print(cell.getStringCellValue() + " \t\t " );
			    		entry.setRequestDate(cell.getStringCellValue());
			            break;
			        case 6:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setClient(cell.getStringCellValue());
			        	break;
			        case 7:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setPhoneA(cell.getStringCellValue());
			        	break;
			        case 8:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setPhoneB(cell.getStringCellValue());
			        	break;
			        case 9:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setCity(cell.getStringCellValue());
			        	break;
			        case 10:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setCounty(cell.getStringCellValue());
			        	break;
			        case 11:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setDistrict(cell.getStringCellValue());
			        	break;
			        case 12:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setQuarter(cell.getStringCellValue());
			        	break;
			        case 13:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setStreet(cell.getStringCellValue());
			        	break;
			        case 14:
			        	//System.out.print(cell.getStringCellValue() + " \t\t ");
			        	entry.setApartmentNo(cell.getStringCellValue());
			        	break;
			        default:
			        	break;
			    }
			    
			    if (!cellIterator.hasNext()) {
			    	count = 1;
			    } else {
			    	count++;
			    }
			}
			
			entry.excelRow = excelRow;
			if (!entry.submitted) {
				entries.add(entry);
			}
			excelRow++;
			//System.out.println();
		}
		
		System.out.println(entries.size() + " Entries Parsed\n");
		return entries;
	}
	
	void close() throws IOException {
		if (workbook != null) {
			workbook.close();
		}
		if (fis != null) {
			fis.close();	
		}
	}
	
}
